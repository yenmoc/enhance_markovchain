﻿using System;

// ReSharper disable once CheckNamespace
namespace UnityModule.EnhanceMarkov
{
    [Serializable]
    public class EnhanceData
    {
        public int star;
        public int probilitySuccess;
        public int probilityFailKeep;
        public int probilityFail;
        public int probilityBoom;

        public EnhanceData(int star, int probilitySuccess, int probilityFailKeep, int probilityFail, int probilityBoom)
        {
            this.star = star;
            this.probilitySuccess = probilitySuccess;
            this.probilityFailKeep = probilityFailKeep;
            this.probilityFail = probilityFail;
            this.probilityBoom = probilityBoom;
        }
    }
}