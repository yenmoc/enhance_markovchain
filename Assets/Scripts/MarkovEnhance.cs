﻿using System;
using System.Linq;
using Markov;
using UnityEngine;
using UnityModule.Utility;
using UnityMUnityModule.Utility;

// ReSharper disable once CheckNamespace
namespace UnityModule.EnhanceMarkov
{
    public enum EnhanceState
    {
        None = 0,
        Success = 1,
        Fail = 2,
        FailKeep = 3,
        Boom = 4
    }

    public class MarkovEnhance : MonoBehaviour
    {
        private MarkovChain<string> _chain;
        private int _currentValue;
        private int _twiceFail;
        private EnhanceState _currentState = EnhanceState.None;
        private EnhanceData[] EnhanceDatas { get; set; }

        public Action onSuccess;
        public Action onFail;
        public Action onFailKeep;
        public Action onBoom;

        public void LoadEnhanceData(string jsonData)
        {
            EnhanceDatas = JsonHelper.FromJson<EnhanceData>(JsonHelper.FixJson(jsonData));
        }

        // ReSharper disable once MemberCanBePrivate.Global
        public bool Request(int value)
        {
            EnhanceData data = null;
            if (value >= EnhanceDatas.Length)
            {
                return false;
            }

            data = EnhanceDatas[value];

            #region Update Chain

            _chain = new MarkovChain<string>(1);

            _chain.Add(new[] {nameof(EnhanceState.Success)}, nameof(EnhanceState.Success), data.probilitySuccess);
            _chain.Add(new[] {nameof(EnhanceState.Success)}, nameof(EnhanceState.Fail), data.probilityFail);
            _chain.Add(new[] {nameof(EnhanceState.Success)}, nameof(EnhanceState.FailKeep), data.probilityFailKeep);
            _chain.Add(new[] {nameof(EnhanceState.Success)}, nameof(EnhanceState.Boom), data.probilityBoom);

            _chain.Add(new[] {nameof(EnhanceState.Fail)}, nameof(EnhanceState.Success), data.probilitySuccess);
            _chain.Add(new[] {nameof(EnhanceState.Fail)}, nameof(EnhanceState.Fail), data.probilityFail);
            _chain.Add(new[] {nameof(EnhanceState.Fail)}, nameof(EnhanceState.FailKeep), data.probilityFailKeep);
            _chain.Add(new[] {nameof(EnhanceState.Fail)}, nameof(EnhanceState.Boom), data.probilityBoom);

            _chain.Add(new[] {nameof(EnhanceState.FailKeep)}, nameof(EnhanceState.Success), data.probilitySuccess);
            _chain.Add(new[] {nameof(EnhanceState.FailKeep)}, nameof(EnhanceState.Fail), data.probilityFail);
            _chain.Add(new[] {nameof(EnhanceState.FailKeep)}, nameof(EnhanceState.FailKeep), data.probilityFailKeep);
            _chain.Add(new[] {nameof(EnhanceState.FailKeep)}, nameof(EnhanceState.Boom), data.probilityBoom);

            _chain.Add(new[] {nameof(EnhanceState.Boom)}, nameof(EnhanceState.Success), data.probilitySuccess);
            _chain.Add(new[] {nameof(EnhanceState.Boom)}, nameof(EnhanceState.Fail), data.probilityFail);
            _chain.Add(new[] {nameof(EnhanceState.Boom)}, nameof(EnhanceState.FailKeep), data.probilityFailKeep);
            _chain.Add(new[] {nameof(EnhanceState.Boom)}, nameof(EnhanceState.Boom), data.probilityBoom);

            #endregion
            
            if (_twiceFail == 2)
            {
                _twiceFail = 0;
                _currentState = EnhanceState.Success;
                return true;
            }

            _currentState = _chain.Chain(new[] {nameof(_currentState)}).Take(1).FirstOrDefault().ParseEnumType<EnhanceState>();
            switch (_currentState)
            {
                case EnhanceState.None:
                    break;
                case EnhanceState.Success:
                    _twiceFail = 0;
                    _currentValue++;
                    onSuccess?.Invoke();
                    break;
                case EnhanceState.Fail:
                    _twiceFail++;
                    _currentValue--;
                    onFail?.Invoke();
                    break;
                case EnhanceState.FailKeep:
                    _twiceFail++;
                    onFailKeep?.Invoke();
                    break;
                case EnhanceState.Boom:
                    _twiceFail = 0;
                    _currentValue = 0;
                    onBoom?.Invoke();
                    break;
                default:
                    _twiceFail = 0;
                    break;
            }

            return true;
        }
    }
}